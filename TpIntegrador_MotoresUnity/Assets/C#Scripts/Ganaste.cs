using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ganaste : MonoBehaviour
{
    private ContadorMonedas scriptContaMonedas;
    private ControlDiamante scriptContaDiamante;
    public Image Img_Winner;

    // Start is called before the first frame update
    void Start()
    {
        scriptContaMonedas = FindObjectOfType<ContadorMonedas>();
        scriptContaDiamante = FindObjectOfType<ControlDiamante>();
        Img_Winner.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(scriptContaMonedas.Contador_monedas == 20 && scriptContaDiamante.tiene_diamante == true)
        {
            Img_Winner.gameObject.SetActive(true);
            StartCoroutine("Esperar");
            
        }
    }

    private IEnumerator Esperar()
    {
        int tiempo_espera = 3;
        while (tiempo_espera > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempo_espera--;
        }
        Application.Quit();
        //UnityEditor.EditorApplication.isPlaying = false;
    }

}
