using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PastilasTiempo : MonoBehaviour
{
    
    private TemporizadorJuego scriptTempoJuego;
    // Start is called before the first frame update
    void Start()
    {
        scriptTempoJuego = FindObjectOfType<TemporizadorJuego>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        this.gameObject.SetActive(false);
        scriptTempoJuego.temporizador += 30;
    }
}
