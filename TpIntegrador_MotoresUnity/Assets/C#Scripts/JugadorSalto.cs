using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class JugadorSalto : MonoBehaviour
{
    int CantSaltosDisponibles;
    float magnitudSalto;
    private Rigidbody miRB;
    // Start is called before the first frame update
    void Start()
    {
        magnitudSalto = 7;
        CantSaltosDisponibles = 2;
        miRB = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.Space) && (CantSaltosDisponibles > 0))
        {
            
            miRB.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            CantSaltosDisponibles -= 1;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Piso")
        {
            CantSaltosDisponibles = 2;
        }
    }
}
