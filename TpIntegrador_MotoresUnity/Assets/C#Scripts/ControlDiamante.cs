using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ControlDiamante : MonoBehaviour
{
    public bool tiene_diamante;
    public TMP_Text txt_Diamante;
    // Start is called before the first frame update
    void Start()
    {
        tiene_diamante = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        this.gameObject.SetActive(false);
        txt_Diamante.text = "Diamante: Si";
        tiene_diamante=true;
    }
}
