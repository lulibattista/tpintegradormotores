using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbreteSesamo : MonoBehaviour
{
    public Rigidbody Puerta;
    public Animation AbrirPuerta;
    private LevantoLlave scriptLevantoLlave;


    // Start is called before the first frame update
    void Start()
    {
        AbrirPuerta = Puerta.GetComponent<Animation>();
        scriptLevantoLlave = FindObjectOfType<LevantoLlave>();
        scriptLevantoLlave.TengolaLlave = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Jugador" && scriptLevantoLlave.TengolaLlave == 1)
        {
            
            AbrirPuerta.Play("AbreteSesamo");
        }
        
    }

}
