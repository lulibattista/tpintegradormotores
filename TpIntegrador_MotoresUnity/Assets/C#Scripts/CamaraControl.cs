using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraControl : MonoBehaviour
{
    public GameObject Jugador;
    public Vector3 offset;
    public Vector2 mouseMirar;
    public Vector2 suavidadV;
    public float movMouseX;
    public float sensisuav;
    public float sensibilidad = 5.0f;
    public float suavizado = 2.0f;
    public float speedH;
    public float speedV;
    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - Jugador.transform.position;
        
    }

    // Update is called once per frame
    void Update()
    {
        movMouseX = Input.GetAxisRaw("Mouse X");
        var md = new Vector2(movMouseX, 0);
        sensisuav = sensibilidad * suavizado;
        md = Vector2.Scale(md, new Vector2(sensisuav, sensisuav));
        suavidadV.x = Mathf.Lerp(suavidadV.x, md.x, 1f / suavizado);
        suavidadV.y = Mathf.Lerp(suavidadV.y, md.y, 1f / suavizado);
        mouseMirar += suavidadV;
        mouseMirar.y = Mathf.Clamp(mouseMirar.y, -90, 90f);
        transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, Vector3.up);
        Jugador.transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, Jugador.transform.up);

    }

    void LateUpdate()
    {
        transform.position = Jugador.transform.position + offset;
    }
}
