using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CierrateSesamo : MonoBehaviour
{
    public Rigidbody Puerta;
    public Animation CerrarPuerta;

    // Start is called before the first frame update
    void Start()
    {
        CerrarPuerta = Puerta.GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        /*if (flagcerrar == true)
        {
            Puerta.transform.position -= transform.right * 10 * Time.deltaTime;
            if (Puerta.transform.position.x < -398)
            {
                flagcerrar = false;
            }
        }*/
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Jugador")
        {
            CerrarPuerta.Play("CierrateSesamo");
        }

    }
}
