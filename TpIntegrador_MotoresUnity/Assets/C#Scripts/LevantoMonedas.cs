using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevantoMonedas : MonoBehaviour
{
    private ContadorMonedas scriptContaMonedas;
    public TMP_Text txt_CantMone;
    public GameObject ObjtoAudioMoneda;
    AudioSource sonido_moneda;
    // Start is called before the first frame update
    void Start()
    {
        scriptContaMonedas = FindObjectOfType<ContadorMonedas>();
        sonido_moneda = ObjtoAudioMoneda.GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        this.gameObject.SetActive(false);
        sonido_moneda.Play();
        scriptContaMonedas.Contador_monedas += 1;
        txt_CantMone.text = "Monedas: " + scriptContaMonedas.Contador_monedas + "/20";
    }

}
