using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JugadorMovimiento : MonoBehaviour
{
    private Rigidbody miRB;
    public int rapidez;
    // Start is called before the first frame update
    void Start()
    {
        miRB = GetComponent<Rigidbody>();
        rapidez = 20;
    }

    // Update is called once per frame
    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidez;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidez;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        miRB.transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
    }
}
