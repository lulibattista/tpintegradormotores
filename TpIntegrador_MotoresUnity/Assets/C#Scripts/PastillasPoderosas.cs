using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PastillasPoderosas : MonoBehaviour
{
    public GameObject prefabExplocion;
    private JugadorMovimiento scriptJugaMov;
    // Start is called before the first frame update
    void Start()
    {
        scriptJugaMov = FindObjectOfType<JugadorMovimiento>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        this.gameObject.SetActive(false);
        GameObject particulas = Instantiate(prefabExplocion, prefabExplocion.transform.position, Quaternion.identity) as GameObject;
        Destroy(particulas, 2);
        scriptJugaMov.rapidez += 20;
    }

}
