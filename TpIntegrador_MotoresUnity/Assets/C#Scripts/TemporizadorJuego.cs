using System.Collections.Specialized;
using System.Collections.Generic;
using System.Collections;
using System.Globalization;
//using System.Runtime.Hosting;
using UnityEngine.UI;

using UnityEngine;
using TMPro;

public class TemporizadorJuego : MonoBehaviour
{
    public TMP_Text txt_cronometro;
    public Image Img_GameOver;
    

    public float temporizador = 0;
    // Start is called before the first frame update
    void Start()
    {
        Img_GameOver.gameObject.SetActive(false);
        StartCoroutine("ComenzarCronometro");
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator ComenzarCronometro()
    {
        temporizador = 200;
        while (temporizador > 0)
        {
            if(txt_cronometro.text != "Ganaste")
            {
                txt_cronometro.text = " Restan " + temporizador + " segundos ";
            }
            yield return new WaitForSeconds(1.0f);
            temporizador--;
        }
        txt_cronometro.fontSize = 30;
        if(txt_cronometro.text != "Ganaste")
        {
            Img_GameOver.gameObject.SetActive(true);
            StartCoroutine("Esperar");
            
        }

    }
    private IEnumerator Esperar()
    {
        int tiempo_espera = 3;
        while (tiempo_espera > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempo_espera--;
        }
        Application.Quit();
        //UnityEditor.EditorApplication.isPlaying = false;
    }
}
