using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

using UnityEngine;

public class GameOver : MonoBehaviour
{
    private PierdoVidasJugador scriptPVJ;
    public Image Img_GameOver;
    
    // Start is called before the first frame update
    void Start()
    {
        //Encontre este codigo en https://gamedevtraum.com/es/videos-del-canal/como-leer-una-variable-que-esta-en-otro-script-en-unity/
        scriptPVJ = FindObjectOfType<PierdoVidasJugador>();
        Img_GameOver.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
        if (scriptPVJ.cantidad_vidas == 0)
        {
            Img_GameOver.gameObject.SetActive(true);
            StartCoroutine("Esperar");
            
        }
    }
    private IEnumerator Esperar()
    {
        int tiempo_espera = 3;
        while (tiempo_espera > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempo_espera--;
        }
        Application.Quit();
        //UnityEditor.EditorApplication.isPlaying = false;
    }
}
