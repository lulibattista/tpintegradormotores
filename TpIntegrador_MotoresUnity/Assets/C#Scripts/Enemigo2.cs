using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo2 : MonoBehaviour
{
    bool subir = true;
    int rapidez = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (subir)
        {
            Subir();

        }
        else
        {
            Bajar();
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Piso")
        {
            subir = true;

        }
        
        
    }

    void Subir()
    {
        transform.position += transform.up * rapidez * Time.deltaTime;
        if (transform.position.y > 13)
        {
            subir = false;
        }
    }
    void Bajar()
    {
        transform.position -= transform.up * rapidez * Time.deltaTime;
        
    }

}
