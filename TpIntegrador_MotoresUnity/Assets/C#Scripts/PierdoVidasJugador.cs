using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PierdoVidasJugador : MonoBehaviour
{
    public int cantidad_vidas = 3;
    public TMP_Text txt_cantidad_Vidas;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Enemigo1" || collision.gameObject.name == "Enemigo2" || collision.gameObject.name == "Enemigo3" 
            || collision.gameObject.name == "Enemigo4" || collision.gameObject.name == "Enemigo5" || collision.gameObject.name == "Enemigo6"
            || collision.gameObject.name == "Enemigo7" || collision.gameObject.name == "Enemigo8")
        {
            cantidad_vidas -= 1;
            txt_cantidad_Vidas.text= "Vidas: " + cantidad_vidas + "/3";


        }
    }
}
