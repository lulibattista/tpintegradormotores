using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo6 : MonoBehaviour
{
    bool derecha = true;
    int rapidez = 20;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (derecha)
        {
            Derecha();

        }
        else
        {
            Izquierda();
        }

    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Pared2")
        {
            derecha = false;

        }
        else if (collision.gameObject.name == "Pared34")
        {
            derecha = true;
        }
    }

    void Izquierda()
    {
        transform.position -= transform.right * rapidez * Time.deltaTime;
    }

    void Derecha()
    {
        transform.position += transform.right * rapidez * Time.deltaTime;
    }


}
